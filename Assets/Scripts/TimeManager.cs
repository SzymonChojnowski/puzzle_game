﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class TimeManager : MonoBehaviour
{

    [SerializeField] private bool _shouldCounting;
    [SerializeField] private bool _coutingUp;
    [SerializeField] private float _timeToPlay = 90.0f;
    private string timeText;
    public float TimeToPlay { get => _timeToPlay; set => _timeToPlay = value; }
    public bool ShouldCounting { get => _shouldCounting; set => _shouldCounting = value; }
    public bool CoutingUp { get => _coutingUp; set => _coutingUp = value; }

    public float Timer { get; set; }
    public TextMeshProUGUI CounterText { get; set; }

    void Update()
    {
        UpdateTime();
    }

    void OnEnable()
    {
        CounterText = GetComponent<TextMeshProUGUI>();
    }

    public void UpdateTime()
    {
        if(ShouldCounting)
        {
            if (CoutingUp)
            {
                Timer += Time.deltaTime;
            }
            else
            {
                Timer -= Time.deltaTime;
            }
            timeText = TimeToString(Timer);
            CounterText.SetText(timeText);
        }
    }

    public string TimeToString(float t)
    {
        string seconds = Mathf.Floor(t % 60f).ToString("00");
        string minutes = Mathf.Floor(t / 60f).ToString("00");
        return minutes + ":" + seconds;
    }

    public void ResetTimer()
    {
        if(CoutingUp)
        {
            Timer = 0;
        }
        else
        {
            Timer = TimeToPlay;
        }
    }

    public void StopTimer()
    {
        _shouldCounting = false;
    }

    public void ResumeTimer()
    {
        _shouldCounting = true;
        if(!CoutingUp)
        {
            Timer = Time.time - TimeToPlay;
        }
    }

    public void StartTimer()
    {
        _shouldCounting = true;
        if(CoutingUp)
        {
            Timer = 0;
        }
        else
        {
            Timer = TimeToPlay;
        }
    }

   public  IEnumerator GameOverChecker(UnityAction action)
    {
        yield return new WaitUntil(() => Timer <= 0.1f);
        StopTimer();
        action.Invoke();
    }

}