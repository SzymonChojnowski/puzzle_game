﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PuzzlePick : MonoBehaviour
{
    private Button puzzlePick;
    private PuzzleBoard _puzzleBoard;

    public PuzzleBoard PuzzleBoard { get => _puzzleBoard; set => _puzzleBoard = value; }

    void Awake()
    {
        puzzlePick = GetComponent<Button>();
        puzzlePick.onClick.AddListener(OnPuzzlePickClick);
    }

    public void ChangePhoto()
    {
        puzzlePick.image.sprite = PuzzleBoard.Photo;
    }

    private void OnPuzzlePickClick()
    {
        GetComponentInParent<PuzzlePicker>().ChangeCurrentBoard(PuzzleBoard);
    }

}
