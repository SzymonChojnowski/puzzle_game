﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public GameObject UI;
    public PuzzlePreview PuzzlePreview;
    public Button StartBtn;
    public TextMeshProUGUI movesText;
    public TextMeshProUGUI moveCounter;
    public TextMeshProUGUI timer;
    public TextMeshProUGUI puzzleName;

    [Header("Win/Lose Screen")]
    public GameObject EndPanel;
    public GameObject winScreen;
    public GameObject loseScreen;
    public TextMeshProUGUI winTime;
    public TextMeshProUGUI winMoves;
    public TextMeshProUGUI loseMissed;
    public TextMeshProUGUI loseMoves;
    public Image puzzlePicture;
}
