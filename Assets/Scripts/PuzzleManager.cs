﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : Singleton<PuzzleManager>
{
    [SerializeField] private PuzzleBoard[] _puzzles;
    [SerializeField] private PuzzlePicker _puzzlePicker;

    public PuzzlePicker PuzzlePicker { get => _puzzlePicker; set => _puzzlePicker = value; }
    public PuzzleBoard[] Puzzles { get => _puzzles; set => _puzzles = value; }

    public PuzzleBoard SpawnPuzzleBoard()
    {
        return Instantiate(PuzzlePicker.CurrentPuzzle, PuzzlePicker.CurrentPuzzle.transform.position, 
            Quaternion.identity);
    }

}
