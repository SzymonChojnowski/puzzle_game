﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private TimeManager timeManager;
    private int _correctPieces = 0;
    private int _moves = 0;

    [HideInInspector] public UIManager UIManager;
    public PuzzleBoard currentPuzzleBoard;
    public PuzzleManager puzzleManager;
    public IGameStates State { get; set; }
    public Piece choosedPiece = null;
    public TimeManager TimeManager { get => timeManager; set => timeManager = value; }
    public int CorrectPieces
    {
        get => _correctPieces;
        set
        {
            _correctPieces = value;
            if (currentPuzzleBoard != null)
            {
                if (_correctPieces == currentPuzzleBoard.BoardSize)
                {
                    State.ToState(this, GameStates.STATE_WIN);
                }
            }
        }
    }
    public int Moves
    {
        get => _moves;
        set
        {
            _moves = value;
            UIManager.moveCounter.SetText(_moves.ToString());
        }
    }

    private void Start()
    {
        UIManager = GetComponent<UIManager>();
        State = GameStates.NO_STATE;
        State.ToState(this, GameStates.STATE_IMG_CHOOSE);
        puzzleManager.PuzzlePicker.LoadPuzzles(puzzleManager.Puzzles);
    }

    private void Update()
    {
        if (choosedPiece != null)
        {
            choosedPiece.GetComponent<Button>().Select();
        }
    }

    public void StartGame()
    {
        currentPuzzleBoard = puzzleManager.SpawnPuzzleBoard();
        TimeManager.StartTimer();
        State.ToState(this, GameStates.STATE_PLAYING);
        AddListeners();
        currentPuzzleBoard.Shuffle();
    }

    public void BackToMenu()
    {
        Destroy(currentPuzzleBoard.gameObject);
        State.ToState(this, GameStates.STATE_IMG_CHOOSE);
    }

    public void TryAgain()
    {
        TimeManager.StartTimer();
        State.ToState(this, GameStates.STATE_PLAYING);
        currentPuzzleBoard.Shuffle();
    }

    private void AddListeners()
    {
        for (int i = 0; i < currentPuzzleBoard.Pieces.Length; i++)
        {
            Button button = currentPuzzleBoard.Pieces[i].GetComponent<Button>();
            button.onClick.AddListener(delegate { OnPuzzleClick(button); });
        }
    }

    private void OnPuzzleClick(Button puzzle)
    {
        State.OnPuzzleClick(this, puzzle);
    }

}
