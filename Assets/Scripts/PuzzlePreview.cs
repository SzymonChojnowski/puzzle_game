﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PuzzlePreview : MonoBehaviour
{

    [SerializeField] private Image previewBox;
    [SerializeField] private TextMeshProUGUI puzzleName;

    public void ChangeData(PuzzleBoard puzzleBoard)
    {
        puzzleName.SetText(puzzleBoard.PhotoName);
        previewBox.sprite = puzzleBoard.Photo;
    }

}
