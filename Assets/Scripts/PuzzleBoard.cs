﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleBoard : MonoBehaviour
{
    [SerializeField] private Piece[] _pieces;
    [SerializeField] private int _id;
    [SerializeField] private string _photoName;
    [SerializeField] private Sprite _photo;
    private int _boardSize;

    public int BoardSize { get => _boardSize; set => _boardSize = value; }
    public Sprite Photo { get => _photo; set => _photo = value; }
    public string PhotoName { get => _photoName; set => _photoName = value; }
    public int Id { get => _id; set => _id = value; }
    public Piece[] Pieces { get => _pieces; set => _pieces = value; }

    private void OnEnable()
    {
        BoardSize = transform.childCount;
        Pieces = new Piece[BoardSize];
        for(int i=0; i < BoardSize; i++)
        {
            Pieces[i] = transform.GetChild(i).GetComponent<Piece>();
            Pieces[i].piecePosition = PiecePosition.Wrong;
            Pieces[i].GetComponent<Button>().interactable = true;
        }
    }

    public List<int> FillList(int start, int end)
    {
        return Enumerable.Range(start, end).ToList();
    }

    public void SwapPuzzles(Piece puzzleToChange, Piece choosedPiece)
    {
        int temp = puzzleToChange.transform.GetSiblingIndex();
        puzzleToChange.transform.SetSiblingIndex(choosedPiece.transform.GetSiblingIndex());
        choosedPiece.transform.SetSiblingIndex(temp);
    }

    public void Shuffle()
    {
        List<int> numberList = FillList(0, 16);
        List<int> unused = new List<int>();
        unused.AddRange(numberList);
        int i = 0;
        while (unused.Count > 0)
        {
            int c = Random.Range(0, unused.Count);
            int current = unused[c];
            if (i != current)
            {
                unused.RemoveAt(c);
                SwapPuzzles(Pieces[i], Pieces[current]);
                i++;

            }
            else if (i == current)
            {
                unused.RemoveAt(c);
                int rnd = Random.Range(0, 15);
                SwapPuzzles(Pieces[i], Pieces[rnd]);
            }
        }
        for (int j = 0; j < BoardSize; j++)
        {
            int position = Pieces[j].transform.GetSiblingIndex();
            if (position == j)
            {
                int rnd = Random.Range(0, 15);
                do
                {
                    rnd = Random.Range(0, 15);
                } while (rnd == position);
                SwapPuzzles(Pieces[j], Pieces[rnd]);
            }
        }
    }
}
