﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Util : Singleton<Util>
{
    public LayerMask PUZZLE;
    public string myPath;

    public bool IsInLayerMask(int layer, LayerMask layermask)
    {
        return layermask == (layermask | (1 << layer));
    }

    public FileInfo[] ReadPuzzleFolder(string currentPuzzle)
    {  
        DirectoryInfo dir = new DirectoryInfo(Application.dataPath + currentPuzzle);
        FileInfo[] info = dir.GetFiles("*.png");
        return info;
    }

    public Sprite LoadNewSprite(FileInfo FilePath, float PixelsPerUnit = 100.0f, SpriteMeshType spriteType = SpriteMeshType.FullRect)
    {
        Texture2D SpriteTexture = LoadTexture(FilePath.FullName);
        Sprite NewSprite = Sprite.Create(SpriteTexture, new Rect(0, 0, SpriteTexture.width, SpriteTexture.height), new Vector2(0, 0), PixelsPerUnit, 0, spriteType);
        return NewSprite;
    }

    public Texture2D LoadTexture(string FilePath)
    {
        Texture2D Tex2D;
        byte[] FileData;

        if (File.Exists(FilePath))
        {
            FileData = File.ReadAllBytes(FilePath);
            Tex2D = new Texture2D(2, 2);           
            if (Tex2D.LoadImage(FileData))          
                return Tex2D;                 
        }
        return null;                     
    }
}
