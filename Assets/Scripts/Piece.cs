﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PiecePosition
{
    Correct,
    Wrong
}

public class Piece : MonoBehaviour
{
    [SerializeField] private int _id;
    public PiecePosition piecePosition = PiecePosition.Wrong;
    public int Id { get => _id; set => _id = value; }
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void SetWinPosition()
    {
        piecePosition = PiecePosition.Correct;
        animator.SetBool("correct", true);
    }

    public bool CheckRightPosition()
    {
        if (Id == transform.GetSiblingIndex())
        {
            SetWinPosition();
            return true;
        }
        return false;
    }
}