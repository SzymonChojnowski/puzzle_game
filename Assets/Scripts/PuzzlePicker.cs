﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePicker : MonoBehaviour
{
    private PuzzlePick[] puzzleChoices;
    private PuzzleBoard _currentPuzzle;
    private PuzzlePreview puzzlePreview;

    public PuzzleBoard CurrentPuzzle { get => _currentPuzzle; set => _currentPuzzle = value; }

    private void Awake()
    {
        puzzlePreview = GetComponentInParent<PuzzlePreview>();
    }

    public void LoadPuzzles(PuzzleBoard[] puzzles)
    {
        puzzleChoices = new PuzzlePick[puzzles.Length];
        for (int i = 0; i < puzzles.Length; i++)
        {
            puzzleChoices[i] = transform.GetChild(i).GetComponent<PuzzlePick>();
            puzzleChoices[i].PuzzleBoard = puzzles[i];
            puzzleChoices[i].ChangePhoto();
        }
        ChangeCurrentBoard(puzzleChoices[0].PuzzleBoard);
    }

    public void ChangeCurrentBoard(PuzzleBoard puzzleBoard)
    {
        CurrentPuzzle = puzzleBoard;
        puzzlePreview.ChangeData(CurrentPuzzle);
    }

}
