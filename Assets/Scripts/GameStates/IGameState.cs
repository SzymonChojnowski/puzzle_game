﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IGameStates
{

    void OnEnter(GameManager controller);

    void OnExit(GameManager controller);

    void ToState(GameManager controller, IGameStates targetState);

    void OnPuzzleClick(GameManager controller, Button puzzle);


}
