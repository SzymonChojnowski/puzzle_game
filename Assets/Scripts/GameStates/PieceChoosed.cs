﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PieceChoosed : GameStates
{
    public override void OnEnter(GameManager controller)
    {
        base.OnEnter(controller);
    }

    public override void OnExit(GameManager controller)
    {
        controller.Moves++;
    }

    public override void OnPuzzleClick(GameManager controller, Button puzzle)
    {
        Piece PuzzleToChange = puzzle.GetComponent<Piece>();
        if (PuzzleToChange.Id != controller.choosedPiece.Id)
        {
            if(PuzzleToChange.piecePosition==PiecePosition.Wrong)
            {
                controller.currentPuzzleBoard.SwapPuzzles(PuzzleToChange, controller.choosedPiece);
                if (PuzzleToChange.CheckRightPosition()) controller.CorrectPieces++;
                if (controller.choosedPiece.CheckRightPosition()) controller.CorrectPieces++;
                controller.choosedPiece.GetComponent<Button>().interactable = false;
                controller.choosedPiece.GetComponent<Button>().interactable = true;
                controller.choosedPiece = null;
                puzzle.interactable = false;
                puzzle.interactable = true;
                if (controller.State != STATE_WIN) ToState(controller, STATE_PLAYING);
            }
            else
            {
                controller.choosedPiece.GetComponent<Button>().Select();
            }

        }
        
    }
}
