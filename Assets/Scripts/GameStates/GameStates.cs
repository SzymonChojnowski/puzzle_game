﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class GameStates : IGameStates
{
    public static readonly IGameStates STATE_IMG_CHOOSE = new ImageChoosing();
    public static readonly IGameStates STATE_PLAYING = new Playing();
    public static readonly IGameStates STATE_PIECE_CHOOSED = new PieceChoosed();
    public static readonly IGameStates STATE_GAMEOVER = new GameOver();
    public static readonly IGameStates STATE_WIN = new Win();
    public static readonly IGameStates NO_STATE = new NoState();

    public virtual void OnEnter(GameManager controller){ }
    public virtual void OnExit(GameManager controller) { }
    public virtual void ToState(GameManager controller, IGameStates targetState)
    {

        controller.State.OnExit(controller);
        controller.State = targetState;
        controller.State.OnEnter(controller);
    }
    public virtual void OnPuzzleClick(GameManager controller, Button puzzle) { }
}
