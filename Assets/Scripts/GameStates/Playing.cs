﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Playing : GameStates
{

    public override void OnEnter(GameManager controller)
    {
        base.OnEnter(controller);
        controller.StartCoroutine(controller.TimeManager.GameOverChecker(() => ToState(controller, STATE_GAMEOVER)));
    }

    public override void OnPuzzleClick(GameManager controller, Button puzzle)
    {
        if (puzzle.GetComponent<Piece>().piecePosition == PiecePosition.Wrong)
        {
            controller.choosedPiece = puzzle.GetComponent<Piece>();
            ToState(controller, STATE_PIECE_CHOOSED);
        }
        else
        {
            puzzle.interactable = false;
            puzzle.interactable = true;
        }
    }

}

