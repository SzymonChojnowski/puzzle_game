﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : GameStates
{

    public override void OnEnter(GameManager controller)
    {
        controller.StopAllCoroutines();
        controller.TimeManager.StopTimer();
        controller.UIManager.loseScreen.SetActive(true);
        controller.UIManager.EndPanel.SetActive(true);
        controller.currentPuzzleBoard.gameObject.SetActive(false);
        controller.UIManager.StartBtn.gameObject.SetActive(false);
        controller.UIManager.movesText.gameObject.SetActive(false);
        controller.UIManager.timer.gameObject.SetActive(false);
        controller.UIManager.puzzleName.gameObject.SetActive(false);
        controller.UIManager.loseMissed.SetText((controller.currentPuzzleBoard.BoardSize - controller.CorrectPieces).ToString());
        controller.UIManager.loseMoves.SetText(controller.Moves.ToString());
        controller.CorrectPieces = 0;
    }

    public override void OnExit(GameManager controller)
    {
        controller.UIManager.loseScreen.SetActive(false);
        controller.UIManager.EndPanel.SetActive(false);
        controller.currentPuzzleBoard.gameObject.SetActive(true);
        controller.UIManager.StartBtn.gameObject.SetActive(true);
        controller.UIManager.movesText.gameObject.SetActive(true);
        controller.UIManager.timer.gameObject.SetActive(true);
        controller.UIManager.puzzleName.gameObject.SetActive(true);
        controller.Moves = 0;
    }

}
