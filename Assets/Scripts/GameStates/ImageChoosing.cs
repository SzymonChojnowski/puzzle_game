﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ImageChoosing : GameStates
{

    public override void OnEnter(GameManager controller)
    {
        controller.StopAllCoroutines();
        controller.UIManager.StartBtn.onClick.RemoveAllListeners();
        controller.UIManager.StartBtn.GetComponentInChildren<TextMeshProUGUI>().SetText("Start");
        controller.UIManager.StartBtn.onClick.AddListener(controller.StartGame);
        controller.UIManager.timer.gameObject.SetActive(false);
        controller.UIManager.movesText.gameObject.SetActive(false);
        controller.UIManager.PuzzlePreview.gameObject.SetActive(true);
        controller.Moves = 0;
        controller.CorrectPieces = 0;

    }

    public override void OnExit(GameManager controller)
    {
        controller.UIManager.StartBtn.onClick.RemoveAllListeners();
        controller.UIManager.PuzzlePreview.gameObject.SetActive(false);
        controller.UIManager.StartBtn.onClick.AddListener(controller.BackToMenu);
        controller.UIManager.timer.gameObject.SetActive(true);
        controller.UIManager.movesText.gameObject.SetActive(true);
        controller.UIManager.StartBtn.GetComponentInChildren<TextMeshProUGUI>().SetText("Menu");
    }
}
