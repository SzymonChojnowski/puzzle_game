﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win : GameStates
{
    public override void OnEnter(GameManager controller)
    {
        controller.TimeManager.StopTimer();
        controller.StopAllCoroutines();
        controller.UIManager.winScreen.SetActive(true);
        controller.currentPuzzleBoard.gameObject.SetActive(false);
        controller.UIManager.StartBtn.gameObject.SetActive(false);
        controller.UIManager.movesText.gameObject.SetActive(false);
        controller.UIManager.timer.gameObject.SetActive(false);
        controller.UIManager.puzzleName.gameObject.SetActive(false);
        controller.UIManager.EndPanel.SetActive(true);
        controller.UIManager.winMoves.SetText(controller.Moves.ToString());
        controller.UIManager.puzzlePicture.GetComponent<Image>().sprite =
            controller.currentPuzzleBoard.Photo;
        controller.UIManager.winTime.SetText(controller.TimeManager.TimeToString(controller.TimeManager.TimeToPlay - controller.TimeManager.Timer));
        controller.CorrectPieces = 0;
    }

    public override void OnExit(GameManager controller)
    {
        controller.UIManager.winScreen.SetActive(false);
        controller.currentPuzzleBoard.gameObject.SetActive(true);
        controller.UIManager.StartBtn.gameObject.SetActive(true);
        controller.UIManager.movesText.gameObject.SetActive(true);
        controller.UIManager.timer.gameObject.SetActive(true);
        controller.UIManager.puzzleName.gameObject.SetActive(true);
        controller.UIManager.EndPanel.SetActive(false);
        controller.UIManager.winMoves.SetText(controller.Moves.ToString());
        controller.Moves = 0;
    }

}
